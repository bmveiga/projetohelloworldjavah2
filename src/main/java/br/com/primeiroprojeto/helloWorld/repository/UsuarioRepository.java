package br.com.primeiroprojeto.helloWorld.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.primeiroprojeto.helloWorld.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, String>{
	
}
