package br.com.primeiroprojeto.helloWorld.model;

import javax.persistence.*;

@Entity
public class Usuario {

	@Id 
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@GeneratedValue é opcional
	private String userId;
	private String senha;
	private String nome;
	private String cpf;
	private String tipo;
	
	public Usuario () {
		
	}
	
	public Usuario(String userId, String senha, String nome, String cpf, String tipo) {
		super();
		this.userId = userId;
		this.senha = senha;
		this.nome = nome;
		this.cpf = cpf;
		this.tipo = tipo;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
