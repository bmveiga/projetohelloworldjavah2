package br.com.primeiroprojeto.helloWorld.form;

import br.com.primeiroprojeto.helloWorld.model.Usuario;

public class UsuarioForm {

	private String userId;
	private String senha;
	private String nome;
	private String cpf;
	private String tipo;
	

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Usuario converter() {
		return new Usuario(userId, senha, nome, cpf, tipo);
	}
}
