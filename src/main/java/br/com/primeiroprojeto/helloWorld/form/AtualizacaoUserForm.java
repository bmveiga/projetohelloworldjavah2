package br.com.primeiroprojeto.helloWorld.form;

import br.com.primeiroprojeto.helloWorld.model.Usuario;

public class AtualizacaoUserForm {

	private String senha;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Usuario atualizar(String userId, Usuario usuarioRepository) {
		Usuario user = usuarioRepository;
		user.setSenha(this.senha);
		
		return user;
	}
	
}