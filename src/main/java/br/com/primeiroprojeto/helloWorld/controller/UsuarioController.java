package br.com.primeiroprojeto.helloWorld.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.primeiroprojeto.helloWorld.dto.UsuarioDTO;
import br.com.primeiroprojeto.helloWorld.form.AtualizacaoUserForm;
import br.com.primeiroprojeto.helloWorld.form.UsuarioForm;
import br.com.primeiroprojeto.helloWorld.model.Usuario;
import br.com.primeiroprojeto.helloWorld.repository.UsuarioRepository;

@RestController
@RequestMapping("/user")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
//	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public List<UsuarioDTO> listar(){
		List<Usuario> usuarios = usuarioRepository.findAll();
		return UsuarioDTO.converter(usuarios) ;
	}
	
	@GetMapping("/{userId}")
	public Usuario buscarUser(@PathVariable String userId) {
		System.out.println("User Id: " + userId);
		return null;
		
	}
	
	@PostMapping
	public Usuario salvar(@RequestBody UsuarioForm userForm) {
		System.out.println("Chegou no post");
		Usuario user = userForm.converter();
		return user;
	}
	
	@PutMapping("/{userId}")
	public Usuario atualizar(@PathVariable String userId, @RequestBody AtualizacaoUserForm userForm) {
		System.out.println("alterando senha");
		Usuario userDoDB = new Usuario ("FX001", "000", "Boco", "99402", "dev");
		Usuario user = userForm.atualizar(userId, userDoDB);
		return user;
	}
	
	@DeleteMapping("/{userId}")
	public Usuario deletar(@PathVariable String userId) {
		System.out.println("chegou no deletar");
		return null;
	}
}
